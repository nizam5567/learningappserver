const { verify } = require("jsonwebtoken");
const User = require('../modules/user/models/user');

const AuthMiddleware = async (req, res, next) => {
    const authHeaders = req.get("Authorization");
    // console.log("AUTH_HEADER", authHeaders);
    if(!authHeaders) {
        req.isAuth = false;
        return next();
    }

    const token = authHeaders.split(' ')[1];
    // console.log("Token", token);

    if(!token || token === '') {
        req.isAuth = false;
        return next();
    }

    let decodedToken;

    try {
        decodedToken = verify(token, "123");
    } catch (err) {
        console.log("error - ", err);
        req.isAuth = false;
        return next();
    }

    if(!decodedToken) {
        req.isAuth = false;
        return next();
    }

    const authUser = await User.findById(decodedToken.id);
    console.log("authenticated user", authUser);
    if(!authUser) {
        req.isAuth = false;
        return next();
    }

    req.user = authUser;
    req.isAuth = true;
    return next();
}

module.exports = AuthMiddleware;