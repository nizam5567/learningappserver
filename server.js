// #1 Import Express and Apollo Server
const express = require('express');
const { ApolloServer } = require('apollo-server-express');

// #2 Import mongoose
const mongoose = require('./config/database');

// #3 Import GraphQL type definitions
const typeDefs = require('./modules/content/typeDefs');

// #4 Import GraphQL resolvers
const resolvers = require('./modules/content/resolvers');
const rootSchema = require("./modules/rootSchema");

const AuthMiddleware = require("./middlewares/auth");
const cors = require('cors');

// #5 Initialize an Apollo server
// const server = new ApolloServer({ typeDefs, resolvers });
const server = new ApolloServer({
  schema: rootSchema,  
  context: ({
    req
  }) => {
    const {
      isAuth,
      user
    } = req;
    return {
      req,
      isAuth,
      user,
      // ...AppModels
    }
  }
});


const app = express();
app.use(AuthMiddleware);

// enable cors
var corsOptions = {
  //origin: 'http://localhost:3000/',
  origin:'*',
  credentials: true, // <-- REQUIRED backend setting
  optionSuccessStatus:200,
};
app.use(cors(corsOptions));

server.start().then(() => {


  // #7 Use the Express application as middleware in Apollo server
  server.applyMiddleware({ app });

  // #8 Set the port that the Express application will listen to
  // app.listen({ port: 3000 }, () => {
  //   console.log(`Server running on http://localhost:${3000}${server.graphqlPath}`);
  // });
});
// #6 Initialize an Express application
// const app = express();

// // #7 Use the Express application as middleware in Apollo server
// server.applyMiddleware({ app });

// // #8 Set the port that the Express application will listen to
app.listen({ port: 4000 }, () => {
  //console.log(`Server running on http://localhost:${port}${server.graphqlPath}`);
  console.log(`Server running on http://localhost:4000${server.graphqlPath}`);
});