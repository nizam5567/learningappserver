const { gql } = require('apollo-server-express');

const sentenceTypeDefs = gql`
  type Sentence {
    _id: String,
    sentenceText: String,  
    sentenceTextBangla: String,
    content_id: String
  },
  
  extend type Query {
    getSentencesByContentID(content_id: String): [Sentence]
  },
`;

module.exports = sentenceTypeDefs;