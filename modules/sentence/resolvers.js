const Sentence = require('./models/sentence');

const sentenceResolvers = {
  Query: {    
    getSentencesByContentID: async(parent, args, {req}) =>  {
      // console.log(args);
      return await Sentence.find({content_id: args.content_id});
    }
  },
};

module.exports = sentenceResolvers;