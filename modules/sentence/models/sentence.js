const { Schema, model }  = require('mongoose');

const sentenceSchema = new Schema({
  _id: { type: Schema.Types.ObjectId },
  sentenceText: String,  
  sentenceTextBangla: String,
  content_id: { type: Schema.Types.ObjectId, ref:"content" },
});

const Sentence = model('sentence', sentenceSchema);

module.exports = Sentence;