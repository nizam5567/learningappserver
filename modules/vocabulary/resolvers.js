const Vocabulary = require('./models/vocabulary');

const vocabularyResolvers = {
  Query: {    
    getVocabulariesByContentID: async(parent, args, {req}) =>  {
      // console.log(args);
      return await Vocabulary.find({content_id: args.content_id});
    }
  },
};

module.exports = vocabularyResolvers;