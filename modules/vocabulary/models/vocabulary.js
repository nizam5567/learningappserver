const { Schema, model }  = require('mongoose');

const vocabularySchema = new Schema({
  _id: { type: Schema.Types.ObjectId },
  word: String,  
  wordBangla: String,
  content_id: { type: Schema.Types.ObjectId, ref:"content" },
});

const Vocabulary = model('vocabulary', vocabularySchema);

module.exports = Vocabulary;