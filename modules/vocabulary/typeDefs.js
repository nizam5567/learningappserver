const { gql } = require('apollo-server-express');

const vocabularyTypeDefs = gql`
  type Vocabulary {
    _id: String,
    word: String,  
    wordBangla: String,
    content_id: String
  },
  
  extend type Query {
    getVocabulariesByContentID(content_id: String): [Vocabulary]
  },
`;

module.exports = vocabularyTypeDefs;