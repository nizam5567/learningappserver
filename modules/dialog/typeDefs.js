// #1 Import the gql method from apollo-server-express
const { gql } = require('apollo-server-express');

// #2 Construct a schema with gql and using the GraphQL schema language

// #3 Define the respective type with three fields
// Note that the _id is created automatically by mongoose
//#4 Define the query type that must respond to 'contents' query
//#5 Define a mutation to add new contents with two required fields
const dialogTypeDefs = gql`
  type Dialog {
    _id: String,
    body: String,  
    bodyBangla: String,
    content: Content
  },
  
  extend type Query {
    getDialogs: [Dialog],
    getDialogByID(id: String): Dialog
  },
`;

module.exports = dialogTypeDefs;