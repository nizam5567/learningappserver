// #1 Import the model created with mongoose
const Dialog = require('./models/dialog');
//var ObjectId = require('mongodb').ObjectId; 
//const mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;
// #2 Create resolver functions to handle GraphQL queries
/**
 * Query resolver "posts" must return values in response to
 * the query "posts" in GraphQL schema.
 */
const dialogResolvers = {
  Query: {
    // Query which returns posts list
    getDialogs: async() => {
       //console.log(await Dialog.find({}));
      return await Dialog.find({});
    },
    getDialogByID: async(parent, content) =>  {
      // console.log(content);
      return await Dialog.findById(ObjectId(content.id));
    }
  },
};

module.exports = dialogResolvers;