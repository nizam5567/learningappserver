// #1 Import the constructor Schema and the model() method
// Note the use of ES6 desctructuring
const { Schema, model }  = require('mongoose');

// #2 Instantiate a schema using mongoose Schema
const dialogSchema = new Schema({
  _id: { type: Schema.Types.ObjectId },
  body: String,  
  bodyBangla: String,
  content: { type: Schema.Types.ObjectId, ref:"content" },
});

const Dialog = model('dialog', dialogSchema);

module.exports = Dialog;