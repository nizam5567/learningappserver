const { Schema, model }  = require('mongoose');

const resultSchema = new Schema({
  //_id: { type: Schema.Types.ObjectId },  
  user_id: { type: Schema.Types.ObjectId, ref:"user" },
  content_id: { type: Schema.Types.ObjectId, ref:"content" },
  points: Number,
  pointsInPercentage: Number
});

const Result = model('result', resultSchema);

module.exports = Result;