const { gql } = require('apollo-server-express');

const resultTypeDefs = gql`
  type Result {
    _id: String,    
    user_id: String,
    content_id: String,  
    points: Int,
    pointsInPercentage: Int,
    content: Content
  },
  
  extend type Query {
    getResultsByUserID: [Result]
  },

  extend type Mutation {
    saveResult(content_id: String, points: Int, pointsInPercentage: Int): Result
  }
`;

module.exports = resultTypeDefs;