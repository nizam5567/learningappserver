const Result = require('./models/result');
const Content = require('../content/models/content');

const resultResolvers = {
  Query: {
    getResultsByUserID: async (parent, args, { req }) => {
      // console.log(args);
      // return await Result.find({ user_id: req.user._id });
      const result = await Result
        .aggregate([
          {
            "$lookup": {
              "from": 'contents',
              "localField": "content_id",
              "foreignField": "_id",
              "as": "content"
            }
          },
          {
            $unwind: {
              path: '$content',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $match: {
              user_id: req.user._id
            }
          }
        ]);

      // console.log("result", result);
      return result;
    }
  },
  Mutation: {
    saveResult: async (parent, args, { req }) => {
      console.log("args", args);

      const res = await Result.findOneAndUpdate(
        { content_id: args.content_id, user_id: req.user.id },
        { 'points': args.points, 'pointsInPercentage': args.pointsInPercentage });
      console.log("res", res);

      if (res) {
        return res;
      }

      const newResult = new Result({
        user_id: req.user._id,
        content_id: args.content_id,
        points: args.points,
        pointsInPercentage: args.pointsInPercentage
      });
      return await newResult.save();
    }
  }
};

module.exports = resultResolvers;