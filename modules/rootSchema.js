//import { mergeSchemas } from 'graphql-tools';
// import contentSchema from './content/schema';
//const contentSchema = require('./content/schema');
const merge = require('lodash');
const { makeExecutableSchema } = require('@graphql-tools/schema');
//const mergeSchemas = require('graphql-tools').mergeSchemas;
const { gql } = require('apollo-server-express');

const contentTypeDefs = require('./content/typeDefs');
const contentResolvers = require('./content/resolvers');

const dialogTypeDefs = require('./dialog/typeDefs');
const dialogResolvers = require('./dialog/resolvers');

const userTypeDefs = require('./user/typeDefs');
const userResolvers = require('./user/resolvers');

const favoriteTypeDefs = require('./favorite/typeDefs');
const favoriteResolvers = require('./favorite/resolvers');

const questionTypeDefs = require('./question/typeDefs');
const questionResolvers = require('./question/resolvers');

const answerTypeDefs = require('./answer/typeDefs');
const answerResolvers = require('./answer/resolvers');

const vocabularyTypeDefs = require('./vocabulary/typeDefs');
const vocabularyResolvers = require('./vocabulary/resolvers');

const sentenceTypeDefs = require('./sentence/typeDefs');
const sentenceResolvers = require('./sentence/resolvers');

const resultTypeDefs = require('./result/typeDefs');
const resultResolvers = require('./result/resolvers');

const baseTypeDefs = gql`
  type Query {
    _empty: String
  }
  type Mutation {
    _empty: String
  }  
`;

const resolvers = {};
const schema = makeExecutableSchema({
  typeDefs: [
    baseTypeDefs, contentTypeDefs, dialogTypeDefs, 
    userTypeDefs, favoriteTypeDefs, answerTypeDefs, 
    questionTypeDefs, vocabularyTypeDefs, sentenceTypeDefs,
    resultTypeDefs
  ],
  resolvers: [
    resolvers, contentResolvers, dialogResolvers,
    userResolvers, favoriteResolvers, answerResolvers,
    questionResolvers, vocabularyResolvers, sentenceResolvers,
    resultResolvers
  ],
});

module.exports = schema;