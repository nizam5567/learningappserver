const { Schema, model }  = require('mongoose');

const favoriteSchema = new Schema({
  //_id: { type: Schema.Types.ObjectId },
  user_id: { type: Schema.Types.ObjectId },  
  content_id: { type: Schema.Types.ObjectId },
});

// contentSchema.virtual('favorits', {
//   ref: 'dialog', //The Model to use
//   localField: '_id', //Find in Model, where localField 
//   foreignField: 'content_id', // is equal to foreignField
// });

const Favorite = model('favorite', favoriteSchema);

module.exports = Favorite;