const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type Favorite {
    _id: String,
    user_id: String,  
    content_id: String,    
  },  
  extend type Query {
    getFavorites: [Favorite],
  }
  extend type Mutation {
    addOrRemoveFavorite(content_id: String!): Favorite,
  }
`;

module.exports = typeDefs;