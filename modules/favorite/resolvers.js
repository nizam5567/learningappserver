const Favorite = require('./models/favorite');

var ObjectId = require('mongoose').Types.ObjectId;
const { AuthenticationError } = require("apollo-server-express");
const isEmpty = require("lodash/isEmpty");

const resolvers = {
  Query: {
    getFavorites: async (_, __, { req }) => {
      return await Favorite.find({});
    },
  },


  Mutation: {
    addOrRemoveFavorite: async (parent, args, { req }) => {
      const removeFavData = await Favorite.deleteOne({ content_id: args.content_id });
      if (removeFavData.deletedCount) {
        return removeFavData;
      }

      const newFavorite = new Favorite({
        user_id: req.user._id,
        content_id: ObjectId(args.content_id)
      });
      return newFavorite.save();
    }
  }
};

module.exports = resolvers;