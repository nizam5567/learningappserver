// #1 Import the model created with mongoose
const Content = require('./models/content');
const UserParticipation = require('./models/userParticipation');
//var ObjectId = require('mongodb').ObjectId; 
//const mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;
const { AuthenticationError } = require("apollo-server-express");
const isEmpty = require("lodash/isEmpty");
// #2 Create resolver functions to handle GraphQL queries
/**
 * Query resolver "posts" must return values in response to
 * the query "posts" in GraphQL schema.
 */
const resolvers = {
  Query: {
    // Query which returns posts list
    getContents: async (_, __, { req }) => {
      //if (isEmpty(req.user)) throw new AuthenticationError("Must authenticate");
      return await Content.find({})
    },
    //getContentByID: async (parent, content) => {
    getContentByID: async (parent, args, { req }) => {
      console.log("args", args);
      if (isEmpty(req.user)) throw new AuthenticationError("Must authenticate");

      let data = await Content.findById(ObjectId(args.id)).populate("dialogs");
      //console.log("data ", data);
      return data;
    },
    countUserParticipateByContentId: async (parent, args, { req }) => {
      const count = await UserParticipation.find({ content_id: args.content_id, user_id: req.user.id }).count();
      console.log("count-", count);
      return count;
    }
  },

  /**
   * Mutation resolver addPost creates a new document in MongoDB
   * in response to the "addPost" mutation in GraphQL schema.
   * The mutation resolvers must return the created object.
   */
  Mutation: {
    addContent: (parent, content) => {
      // Create a new record in the database
      const newContent = new Content({ title: content.title });
      // Save the record and return it
      return newContent.save();
    },
    saveUserParticipation: async (parent, args, { req }) => {
      const data = await UserParticipation.findOne({ content_id: args.content_id, user_id: req.user.id });
      //console.log("hasdata-", data);
      if (data) {
        return data;
      }

      await Content.findOneAndUpdate({ _id: args.content_id },
        { $inc: { 'countUsersParticipate': 1 } });

      const newUserParticipation = new UserParticipation({
        user_id: req.user._id,
        content_id: ObjectId(args.content_id)
      });
      return newUserParticipation.save();
    } 
  }
};

module.exports = resolvers;