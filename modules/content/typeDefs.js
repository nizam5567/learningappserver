// #1 Import the gql method from apollo-server-express
const { gql } = require('apollo-server-express');

// #2 Construct a schema with gql and using the GraphQL schema language

// #3 Define the respective type with three fields
// Note that the _id is created automatically by mongoose
//#4 Define the query type that must respond to 'contents' query
//#5 Define a mutation to add new contents with two required fields
const typeDefs = gql`
  type Content {
    _id: String,
    title: String,  
    tileColor: String,
    shortDesc: String,
    countUsersParticipate: Int,
    dialogs: [Dialog]  
  },
  
  extend type Query {
    getContents: [Content],
    getContentByID(id: String): Content,
    countUserParticipateByContentId(content_id:String!): Int
  },

  type UserParticipation {
    _id: String,
    user_id: String,  
    content_id: String,    
  },
  
  extend type Mutation {
    addContent(title: String!): Content,
    saveUserParticipation(content_id: String!): UserParticipation,
  }
`;

module.exports = typeDefs;