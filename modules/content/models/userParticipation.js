const { Schema, model }  = require('mongoose');

const userParticipationSchema = new Schema({
  //_id: { type: Schema.Types.ObjectId },
  user_id: { type: Schema.Types.ObjectId },  
  content_id: { type: Schema.Types.ObjectId },
});

const UserParticipation = model('userParticipation', userParticipationSchema);

module.exports = UserParticipation;