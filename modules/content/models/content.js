// #1 Import the constructor Schema and the model() method
// Note the use of ES6 desctructuring
const { Schema, model }  = require('mongoose');

// #2 Instantiate a schema using mongoose Schema
const contentSchema = new Schema({
  _id: { type: Schema.Types.ObjectId },
  title: String,  
  tileColor: String,
  shortDesc: String,
  countUsersParticipate: Number,
  //dialogs : [{ type: Schema.Types.ObjectId, ref: 'dialog' }]
});

contentSchema.virtual('dialogs', {
  ref: 'dialog', //The Model to use
  localField: '_id', //Find in Model, where localField 
  foreignField: 'content_id', // is equal to foreignField
});

// #3 Create a model with mongoose model() method
const Content = model('content', contentSchema);

module.exports = Content;