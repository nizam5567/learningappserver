const { Schema, model }  = require('mongoose');

const questionSchema = new Schema({
  _id: { type: Schema.Types.ObjectId },
  title: String,  
  content_id: { type: Schema.Types.ObjectId },
  type: String,
  sequence: Number,  
});

questionSchema.virtual('answers', {
  ref: 'answer', //The Model to use
  localField: '_id', //Find in Model, where localField 
  foreignField: 'question_id', // is equal to foreignField
});

const Question = model('question', questionSchema);

module.exports = Question;