const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type Question {
    _id: String,
    title: String,  
    content_id: String, 
    type: String, 
    sequence: Int,
    answers: [Answer]  
  },  
  extend type Query {
    getQuestions: [Question],
    getQuestionsByContentID(content_id: String): [Question]
  }
`;

module.exports = typeDefs;