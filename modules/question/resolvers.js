const Question = require('./models/question');

var ObjectId = require('mongoose').Types.ObjectId;
const { AuthenticationError } = require("apollo-server-express");
const isEmpty = require("lodash/isEmpty");

const resolvers = {
  Query: {
    getQuestions: async (_, __, { req }) => {
      return await Question.find({}).populate("answers");
    },

    getQuestionsByContentID: async (parent, args, { req }) => {
      console.log("args", args);
     //if (isEmpty(req.user)) throw new AuthenticationError("Must authenticate");

     let data = await Question.find({content_id: ObjectId(args.content_id)}).populate("answers");
     //console.log("data ", data);
     return data;
   }
  },

};

module.exports = resolvers;