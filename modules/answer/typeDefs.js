const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type Answer {
    _id: String,
    title: String,  
    question_id: String, 
    isCorrect: Boolean,     
    sequence: Int,  
    ansExplanation: String,
  },    
`;

module.exports = typeDefs;