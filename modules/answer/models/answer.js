const { Schema, model }  = require('mongoose');

const answerSchema = new Schema({
  _id: { type: Schema.Types.ObjectId },
  title: String,  
  question_id: { type: Schema.Types.ObjectId, ref:"question" },
  sequence: Number,  
  isCorrect: Boolean,
  ansExplanation: String,
});

// contentSchema.virtual('favorits', {
//   ref: 'dialog', //The Model to use
//   localField: '_id', //Find in Model, where localField 
//   foreignField: 'content_id', // is equal to foreignField
// });

const Answer = model('answer', answerSchema);

module.exports = Answer;