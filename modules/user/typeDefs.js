const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type User {
    _id: ID,
    email: String,  
    firstName: String,
    lastName: String,
    password: String
  },

  type AuthResp {
    user: User,
    token: String
  },

  extend type Query {
    getUserInfo: User
  },

  extend type Mutation {
    authenticateUser(email: String!, password: String!): AuthResp!
  }
`;

module.exports = typeDefs;