const User = require('./models/user');

const { ApolloError } = require("apollo-server-errors");
const { issueToken, serializeUser } = require("../../functions/UserFunctions");

const resolvers = {
  Query: {    
    getUserInfo: async (parent, args, { req }) => {      
      return req.user;
    },
  },
  Mutation: {
    authenticateUser: async (_, {
      email,
      password
    }, {

    }) => {
      let user;
      try {
        user = await User.findOne({ email });
        // console.log("user -", user);
        if (!user) {
          throw new Error("Email not found.");
        }

        const isMatch = password === user.password;
        if (!isMatch) {
          throw new Error("Invalid password");
        }

      } catch (err) {
        throw new ApolloError(err.message, 403);
      }

      user = user?.toObject();

      user.id = user._id;
      user = serializeUser(user);
      console.log("user -", user);

      const token = await issueToken(user);
      console.log(token);
      return {
        user,
        token
      }
    }

  },

};

module.exports = resolvers;