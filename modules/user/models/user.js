const { Schema, model }  = require('mongoose');

const userSchema = new Schema({
  _id: { type: Schema.Types.ObjectId },
  email: String,  
  firstName: String,
  lastName: String,
  password: String
});

const User = model('user', userSchema);

module.exports = User;