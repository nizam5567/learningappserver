const { sign } = require("jsonwebtoken");
const { pick } = require('lodash');

const issueToken = async (user) => {
    const token = await sign(user, "123", {
        expiresIn: 60 * 60 * 24
    });
    return `Bearer ${token}`;
};

const serializeUser = (user) => {
    console.log(user);
    return pick(user, ['id', 'email'])
};

module.exports = {
    issueToken: issueToken,
    serializeUser: serializeUser
};