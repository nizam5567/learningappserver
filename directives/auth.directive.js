const { defaultFieldResolver } = require("graphql");
const { ApolloError } = require("apollo-server-express");
const { SchemaDirectiveVisitor } = require("graphql-tools");

class IsAuthDirective extends SchemaDirectiveVisitor {
    visitFieldDefinition(field) {
        const {
            resolve = defaultFieldResolver
        } = field;

        field.resolve = async function (...args) {
            const test = args;
            console.log("Test", test);
        }
    }
}

module.exports = IsAuthDirective;